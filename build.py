import json

def convert_quotes(html: str):
    """Replace dumb quotes with smart quotes in HTML."""
    
    # set flags to false.
    # we assume that the HTML is well-formed and that we are starting at the beginning.
    in_html_tag = False
    in_quote = False
    
    # convert all quotes to basic quotes. this might not be necessary,
    # but it is if there is any chance the quotes in the document need
    # to be corrected.
    html = html.replace('&ldquo;','"')
    html = html.replace('&rdquo;','"')
    
    formatted = ""
    
    # iterate over each character, tracking whether inside tag or inside quote
    for c in html:

        if c == '<' or c == '>':
            # go into tag mode.
            in_html_tag = not in_html_tag
            formatted += c
            continue
        
        if c == '"' and not in_html_tag:
            # If we are in a quotation, end the quotation. Else, start the quotation.
            if in_quote:
                formatted += "”"
            else:
                formatted += "“"
            in_quote = not in_quote
            continue
        
        # base case, just add the character.
        formatted += c
            
    return formatted

JSON_FILENAME = 'data.json'
data = json.load(open(JSON_FILENAME))

with open('compress', 'w') as compress_file:
    for _, value in data.items():
        անուն = value['name']
        file_name = անուն.lower().replace(' ', '_')
        նյութ_original = open('նյութեր/'+file_name).read()
        typos = value.get("typos2", [])
        for typo in typos:
            նյութ_original = նյութ_original.replace(typo[0], typo[1])

        նյութ = convert_quotes(նյութ_original)
        html = f'''<!doctype html>
<html lang="hy">
<head>
    <title>{անուն}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{անուն}">
    <link rel="icon" href="իխտուս2.svg">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Sans+Armenian:wdth@62.5&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Sans+Display:wdth@62.5&display=swap">
    <link rel="stylesheet" href="style.css">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src = "isInViewport.min.js"></script>
    <script src = "lifecycle.es5.js"></script>
    <script src = "bible.js"></script>
</head>
<body onload="restoreLocation()">
<main>
<h1>{անուն}</h1>
{նյութ}
<footer><a href=".">֍</a></footer></main></body></html>
'''
        with open('public/' + file_name, "w") as file:
            file.write(html)
        
        compress_file.write('public/' + file_name + '\n')
