function restoreLocation(){
    var restoretarget;

    try { restoretarget = localStorage["last-viewed"] } catch(e) { return }
    if (restoretarget==undefined) return;
    document.getElementById(restoretarget).scrollIntoView();
}

$(document).ready(function()
{
    //when page unloads save URL of topmost item in view
    function saveRestoreState()
    {
        var topid = $('span:in-viewport:first').attr('id');
        try { localStorage["last-viewed"] = topid } catch(e) {}
    }
    lifecycle.addEventListener('statechange', function(event)
    {
        if ((event.oldState == 'passive') && (event.newState == 'hidden'))
            saveRestoreState();
    },true);

});
